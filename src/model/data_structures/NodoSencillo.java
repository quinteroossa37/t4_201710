package model.data_structures;

public class NodoSencillo <T>{
	
	private T item;
	private NodoSencillo<T> siguiente;
	
	public NodoSencillo()
	{
		
	}
	
	public T getNodo()
	{
		return item;
	}
	
	
	public NodoSencillo<T> getSiguiente()
	{
		return siguiente;
	}
	
	public  void setNodo(T nNodo)
	{
		item = nNodo;
	}
	
	
	public void setSiguiente(NodoSencillo<T> nSiguiente)
	{
		siguiente= nSiguiente;
	}
	
}
