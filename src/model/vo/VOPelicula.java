package model.vo;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;

public class VOPelicula implements Comparable{

	private String titulo;
	private int agnoPublicacion;
	private ILista<String> generosAsociados;
	
	public VOPelicula(String nombre, int anio, ListaEncadenada<String> listaGeneros)
	{
		// TODO Auto-generated constructor stub
		titulo = nombre;
		agnoPublicacion = anio;
		generosAsociados = listaGeneros;
		
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
	

}
